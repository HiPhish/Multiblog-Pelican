.. default-role:: code

#########################################
Multiblog - multiple blogs on one website
#########################################


Multiblog is a Pelican plugin that lets you have more than one blog on your
Pelican-generated website. A *blog* consists of the index page, individual
articles (blogposts), a tags page, archives, an authors page, and so on. If a
blog does not have a component specified the default value is used.




How to use
##########

Define your variables the usual way in your `pelicanconf.py` file, these will
serve as the defaults. Then define a `MULTIBLOG` variable as a list of
dictionaries. If `MULTIBLOG` is not defined Multiblog will not do anything,
that way it will not interfere with normal projects.

Here is an example, the keys must be strings and match the names of the
variables they override:

.. code:: python

   # The defaults
   ARTICLE_PATHS = [
     'articles/main/blog',
     'articles/personal/blog',
     'articles/super-app/blog',
     'articles/awesome-app/blog',
   ]

   MULTIBLOG = [
     # Main blog
     {
       'ARTICLE_PATHS'   : ['articles/main/blog'],
       'ARTICLE_URL'     : 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/',
       'ARTICLE_SAVE_AS' : 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html',
       'INDEX_SAVE_AS'   : 'blog/index.html',
     },
     # Personal stuff
     {
       'ARTICLE_PATHS'   : ['articles/personal/blog'],
       'ARTICLE_URL'     : 'personal/blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/',
       'ARTICLE_SAVE_AS' : 'personal/blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html',
       'INDEX_SAVE_AS'   : 'personal/blog/index.html',
     },
     # A commercial project
     {
       'ARTICLE_PATHS'   : ['articles/super-app/blog'],
       'ARTICLE_URL'     : 'super-app/news/{date:%Y}/{date:%m}/{date:%d}/{slug}/',
       'ARTICLE_SAVE_AS' : 'super-app/news/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html',
       'INDEX_SAVE_AS'   : 'super-app/news/index.html',
     },
     # Another commercial project
     {
       'ARTICLE_PATHS'   : ['articles/awesome-app/blog'],
       'ARTICLE_URL'     : 'awesome-app/news/{date:%Y}/{date:%m}/{date:%d}/{slug}/',
       'ARTICLE_SAVE_AS' : 'awesome-app/news/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html',
       'INDEX_SAVE_AS'   : 'awesome-app/news/index.html',
     },
     # Commercial projects in general
     {
       'ARTICLE_PATHS'   : ['articles/super-app/blog','articles/awesome-app/blog'],
       'ARTICLE_URL'     : 'products/news/{date:%Y}/{date:%m}/{date:%d}/{slug}/',
       'ARTICLE_SAVE_AS' : 'products/news/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html',
       'INDEX_SAVE_AS'   : 'products/news/index.html',
     },
     # One blog for all articles
     {
       'ARTICLE_URL'     : 'news/{date:%Y}/{date:%m}/{date:%d}/{slug}/',
       'ARTICLE_SAVE_AS' : 'news/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html',
       'INDEX_SAVE_AS'   : 'news/index.html',
     },
   ]

The main blog will accessible as `my-website.com/blog/`, the personal blog as
`my-website.com/personal/blog/` and so on. We also have an aggregate blog for
all our commercial projects by re-using the articles of two different blogs.
Multiblog does not know that those articles were already part of another blog,
nor does it have to.

The last blog is interesting: it is an aggregate blog like the previous one,
but it does not define the `ARTICLE_PATHS` variable. This means it will use the
default we defined above, which happens to be all our articles.

Note that the above example will *not* work because some pages like the tags
page would be shared between blogs, overwriting each other. Pelican throws and
error in that case. I have omitted these variables for brevity, but you will
have to define all variables that might cause overwriting of files.


Blog-specific variables
=======================

The following tables lists all the variables specific to blogs. You don't have
to define all of them, only the ones that end in `SAVE_AS`. Please consult the
Pelican documentation on the purpose and default value of these variables.

=========================  ==========
 Variable                   Required 
=========================  ==========
 `ARTICLE_PATHS`                  no 
 `ARTICLE_URL`                    no 
 `ARTICLE_SAVE_AS`               yes 
 `ARTICLE_LANG_URL`               no 
 `ARTICLE_LANG_SAVE_AS`          yes 
 `DRAFT_URL`                      no 
 `DRAFT_SAVE_AS`                 yes 
 `DRAFT_LANG_URL`                 no 
 `DRAFT_LANG_SAVE_AS`            yes 
 `CATEGORY_URL`                   no 
 `CATEGORY_SAVE_AS`              yes 
 `TAG_URL`                        no 
 `TAG_SAVE_AS`                   yes 
 `AUTHOR_URL`                     no 
 `AUTHOR_SAVE_AS`                yes 
 `ARCHIVES_SAVE_AS`              yes 
 `YEAR_ARCHIVE_SAVE_AS`          yes 
 `MONTH_ARCHIVE_SAVE_AS`         yes 
 `DAY_ARCHIVE_SAVE_AS`           yes 
 `AUTHORS_SAVE_AS`               yes 
 `CATEGORIES_SAVE_AS`            yes 
 `INDEX_SAVE_AS`                 yes 
 `TAGS_SAVE_AS`                  yes 
=========================  ==========

The exception is if a required variable is set to an empty string (`''`), which
means that page won't be created. In that case it is OK to omit the variable
and use the default empty string value.


Compatibility and shortcomings
==============================

Pelican generators are not designed to run multiple times, therefore this
plugin has some shortcomings:

- Generating links between blogs does not work.
- Plugins relying on the articles generator to always have the same values
  won't work because values change between runs.

This first issue is because order is important in Pelican: the generator
creates a context, reads the files and writes the input. Files are written
after all sources have been read, but in the case of Multiblog we write HTML
pages after the sources of the *current* blog have been read, but before the
sources of the *next* blog are read.

The other issue is that the values of the generator are reset between runs.
That's not a problem if a plugin only requires values to remain constant within
one run, but it is if the values need to be constant even after the run. The
settings will be the settings of the last blog, with no memory of previous
blogs.

The good news is that the signals are not being touched, so plugins that rely
on signals will work fine. Just keep in mind that signals sent by the articles
generator will be sent for each run, which might screw up plugins that would
write to the same file in each run. Overwriting a file is a runtime error in
Pelican.


Static files
============

Static files, such as images linked in an article, need to be in the
`STATIC_PATHS` settings variable, as usual. It is not possible to specify a
blog-specific list in Multiblog. This is due to the fact that in Pelican static
content and articles are handled by different generators.



License
#######

The MIT License (MIT)

Copyright (c) 2015 HiPhish

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
