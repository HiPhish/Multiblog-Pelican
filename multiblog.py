# -*- coding: utf-8 -*-

from __future__  import print_function
from collections import defaultdict
from pelican     import signals

numberOfBlogs = 1
"""Number of blogs defined in the `MULTIBLOG` variable."""

currentBlog = 0
"""The blog we are currently generating."""

multiblog = None
"""List of dictionaries of multiblog settings."""

defaultSettings = None
"""Store default settings to restore later."""

def grabGenerator(generator):
    """Hook up the generator after it has been initialized.
    
    :param generator:  Articles generator object passed by signal.
    """

    global multiblog
    global numberOfBlogs
    global currentBlog
    global defaultSettings

    signals.article_generator_init.disconnect(grabGenerator)
    multiblog = generator.settings.get('MULTIBLOG')

    if not multiblog:
        return

    numberOfBlogs = len(multiblog)
    currentBlog   = 0

    # Save default settings to restore later
    defaultSettings = generator.settings

    updateGenerator(generator, currentBlog)
    signals.article_writer_finalized.connect(rewindGenerator)


def rewindGenerator(generator, writer):
    """Restart the generator, set settings to the next blog.
    If we have reached the last blog the function will be disconnected from the
    signal and the generator can be terminated normally.
    
    :param generator:  Articles generator object passed by signal.
    :param writer:     Articles writer object passed by signal.
    """

    global multiblog
    global numberOfBlogs
    global currentBlog

    currentBlog += 1
    if (currentBlog < numberOfBlogs):
        updateGenerator(generator, currentBlog)
        generator.generate_context()

        generator.generate_output(writer)
    else:
        # Restored settings back to default and disconnect
        signals.article_writer_finalized.disconnect(rewindGenerator)


def updateGenerator(generator, blog):
    """Update the generator's settings

    :param generator:  Articles generator object passed by signal.
    :param blog:       Index of the current blog into the ``multiblog`` list.
    """
    global multiblog

    """Updates the generator's settings dictionary."""
    generator.settings.update(multiblog[blog])
    # Reset these properties manually; they are recycled between runs and
    # would cause crashes if not reset manually. These lines are copied
    # from ``ArticlesGenerator.__init__``.
    generator.articles = []  # only articles in default language
    generator.translations = []
    generator.dates = {}
    generator.tags = defaultdict(list)
    generator.categories = defaultdict(list)
    generator.related_posts = []
    generator.authors = defaultdict(list)
    generator.drafts = [] # only drafts in default language
    generator.drafts_translations = []

    # Re-generate the context and write output
    generator.context.update(multiblog[currentBlog])

def restoreGenerator(generator):
    """Set the generator's settings back to default"""
    generator.settings.update(defaultSettings)


def register():
    signals.article_generator_init.connect(grabGenerator)

